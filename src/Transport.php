<?php

declare(strict_types=1);

namespace Eicc\Fwq\Transport\Database;

use Eicc\Fwq\Exceptions\QueueEmptyException;
use Eicc\Fwq\Exceptions\QueueAlreadyExistsException;
use Eicc\Fwq\Exceptions\InvalidQueueException;
use Eicc\Fwq\Models\AbstractTransport;
use Pimple\Container;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;

/**
 * This is the Database Trasnport.
 */
class Transport extends AbstractTransport
{
  protected ?Container $container;
  protected array $connectionParams = [];
  protected ?Connection $connection = null;

  public function __construct(Container $container)
  {
    $this->container = $container;

    $this->connectionParams = [
        'dbname' => $_ENV['TRANSPORT_DATABASE_DATABASE'],
        'user' => $_ENV['TRANSPORT_DATABASE_USER'],
        'password' => $_ENV['TRANSPORT_DATABASE_PASSWORD'],
        'host' => $_ENV['TRANSPORT_DATABASE_HOST'],
        'driver' => 'pdo_' . $_ENV['TRANSPORT_DATABASE_TYPE'],
        'port' => $_ENV['TRANSPORT_DATABASE_PORT']
    ];

    $this->connection = DriverManager::getConnection($this->connectionParams);
  }

  /**
   * @todo Bad hoodo, all these field names are hard coded. Maybe we do need a
   *       luw object that would know how to build the values array.
   */
  public function push(object $luw, \DateTimeImmutable $runAfter, string $queueName): void
  {
    if (! $this->doesQueueExist($queueName)) {
      $this->container['log']->error($queueName . ' is an invlaid queue name.');
      throw new InvalidQueueException($queueName . ' is an invlaid queue name.');
    }

    $this->connection->createQueryBuilder()
      ->insert('job')
      ->values(
          [
          "client_job_id" => ":client_job_id",
          "date_originally_submitted" =>  ":date_originally_submitted",
          "max_retries" =>  ":max_retries",
          "retries" =>  ":retries",
          "next_retry" =>  ":next_retry",
          "class_name" =>  ":class_name",
          "queue_id" =>  ":queue_id",
          "parameters" => ":parameters"
          ]
      )
    ->setParameter("client_job_id", $luw->clientJobId)
    ->setParameter("date_originally_submitted", $luw->dateOriginallySubmitted)
    ->setParameter("max_retries", $luw->maxRetries)
    ->setParameter("retries", $luw->retries)
    ->setParameter("next_retry", $luw->nextRetry)
    ->setParameter("class_name", $luw->className)
    ->setParameter("queue_id", $this->fetchQueueId($luw->queueName))
    ->setParameter("parameters", json_encode($luw->parameters))
    ->executeStatement();
  }

  /**
   * @todo check runAfter to make sure we honor jobs that aren't yet ready to run.
   */
  public function pop(string $queueName, string $workerName): object
  {
    $this->container['log']->debug('TRANSPORT: BEGIN POPPING a job for ' . $queueName . ':' . $workerName);
    $qb = $this->connection->createQueryBuilder();

    do {
      $this->container['log']->debug('Querying the database for a job');

      $nextJob =
      $query = $this->connection->createQueryBuilder()
        ->select("j.job_id")
        ->from('job', 'j')
        ->leftJoin('j', 'queue', 'q', 'j.queue_id = q.queue_id')
        ->where(
            $qb->expr()->and(
                $qb->expr()->lte('j.next_retry', ':next_retry'),
                $qb->expr()->eq('q.queue_name', ':queue_name'),
                $qb->expr()->isNull('j.locked')
            )
        )
        ->setFirstResult(0)
        ->setMaxResults(1)
        ->setParameter('queue_name', $queueName)
        ->setParameter('next_retry', (new \DateTimeImmutable())->format($_ENV['DATE_FORMAT']));

      switch ($this->getMeta($queueName)->direction) {
        case 'FIFO':
          $query->orderBy('next_retry', 'ASC');
            break;
        case 'LIFO':
        default:
          $query->orderBy('next_retry', 'DESC');
      }


      $nextJob = $query->fetchOne();

      if ($nextJob < 1) {
        $this->container['log']->info($queueName . ' is empty or does not have any jobs ready to run.');
        throw new QueueEmptyException($queueName . ' is empty or does not have any jobs ready to run.');
      }

      $this->connection->createQueryBuilder()->update('job')
        ->set('locked', ':locked')
        ->where('job_id = :job_id')
        ->setParameter('job_id', $nextJob)
        ->setParameter('locked', $workerName)
        ->executeQuery();

      $job = $this->connection->createQueryBuilder()->select(
          "j.job_id",
          "j.client_job_id",
          "j.date_originally_submitted",
          "j.max_retries",
          "j.retries",
          "j.next_retry",
          "j.class_name",
          "j.parameters",
          "j.queue_id",
          'j.locked',
          'q.queue_name'
      )
        ->from('job', 'j')
        ->leftJoin('j', 'queue', 'q', 'j.queue_id = q.queue_id')
        ->where('j.job_id = :job_id')
        ->setParameter('job_id', $nextJob)
        ->fetchAssociative();
    } while ($job['locked'] !== $workerName);

    $this->connection->createQueryBuilder()->delete('job')
      ->where('job_id=:job_id')
      ->setParameter('job_id', $nextJob)
      ->executeStatement();

    $returnValue = new \StdClass();
    $returnValue->jobId = (int)$job['job_id'];
    $returnValue->clientJobId = $job['client_job_id'];
    $returnValue->dateOriginallySubmitted = $job['date_originally_submitted'];
    $returnValue->maxRetries = (int)$job['max_retries'];
    $returnValue->retries = (int)$job['retries'];
    $returnValue->nextRetry = $job['next_retry'];
    $returnValue->className = $job['class_name'];
    $returnValue->queueName = $job['queue_name'];
    $returnValue->parameters = json_decode($job['parameters']);

    $this->container['log']->debug('END POPPING a job for ' . $queueName);

    return $returnValue;
  }

  public function jobList(string $queueName): array
  {
    return $this->connection->createQueryBuilder()
      ->select(
          "j.job_id",
          "j.client_job_id"
      )
        ->from('job', 'j')
        ->leftJoin('j', 'queue', 'q', 'j.queue_id = q.queue_id')
        ->where('q.queue_name = :queue_name')
        ->orderBy('next_retry', 'DESC')
        ->setParameter('queue_name', $queueName)
        ->fetchAllAssociative();
  }

  public function jobCount(string $queueName): int
  {
    return count($this->jobList($queueName));
  }

  public function getSetting(string $queueName, string $key)
  {
    return $this->connection->createQueryBuilder()
      ->select($key)
      ->from('queue')
      ->where('queue_name = :queue_name')
      ->setParameter('queue_name', $queueName)
      ->fetchOne();
  }

  public function setSetting(string $queueName, string $key, $value): void
  {
    $this->connection->createQueryBuilder()
      ->update('queue')
      ->set($key, ':' . $key)
      ->where('queue_name = :queue_name')
      ->setParameter($key, $value)
      ->setParameter('queue_name', $queueName)
      ->executeStatement();
  }

  public function createQueue(string $queueName, array $parameters): void
  {

    if ($this->doesQueueExist($queueName)) {
      throw new QueueAlreadyExistsException('The queue ' . $queueName . ' already exists.');
    }

    $this->connection->createQueryBuilder()
      ->insert('queue')
      ->values(
          [
            'queue_name' => ':queue_name',
            'direction' => ':direction',
          ]
      )
    ->setParameter('queue_name', $queueName)
    ->setParameter('direction', $parameters['direction'])
    ->executeStatement();
  }

  public function destroyQueue(string $queueName): void
  {
    if ($queueName === $_ENV['FAILED_JOB_QUEUE']) {
      throw new InvalidQueueException(' Cannot destroy ' . $_ENV['FAILED_JOB_QUEUE'] . '.');
    }
    if (! $this->doesQueueExist($queueName)) {
      throw new InvalidQueueException($queueName . ' is not a valid queue name.');
    }

    $this->connection->createQueryBuilder()
      ->delete('queue')
      ->where('queue_name = :queue_name')
      ->setParameter('queue_name', $queueName)
      ->executeStatement();
  }

  public function doesQueueExist(string $queueName): bool
  {
    return $this->fetchQueueId($queueName) > 0;
  }

  public function listQueues(): array
  {
    return $this->connection->createQueryBuilder()
      ->select('queue_id', 'queue_name')
      ->from('queue')
      ->fetchAllKeyValue();
  }

  public function getMeta(string $queueName): object
  {
    $metaArray = $this->connection->createQueryBuilder()
    ->select(implode(',', array_keys($this->meta)))
    ->from('queue')
    ->where('queue_name = :queue_name')
    ->setParameter('queue_name', $queueName)
    ->fetchAssociative();

    $returnValue = new \StdClass();
    $returnValue->name = $queueName;

    foreach ($this->meta as $meta => $defaultValue) {
      $returnValue->$meta = isset($metaArray['$meta']) ? $metaArray['$meta'] : $defaultValue;
    }

    return $returnValue;
  }

  public function initalize(): void
  {
    $tables = [];
    foreach ($this->connection->getSchemaManager()->listTables() as $table) {
      $tables[] = $table->getName();
    }

    if (in_array('job', $tables) || in_array('queue', $tables)) {
      throw new \Exception('System already initalized.');
    }

    $sql = [];
    /*
     * Queue
     */
    $schema = new Schema();
    $queue = $schema->createTable("queue");
    $queue->addColumn("queue_id", "bigint", ["unsigned" => true, 'notnull' => true,'autoincrement' => true ]);
    $queue->addColumn("queue_name", "string", ["length" => 100, 'notnull' => true]);
    $queue->addColumn("direction", "string", ["length" => 10, 'notnull' => true]);
    $queue->setPrimaryKey(['queue_id']);
    $sql = array_merge($sql, $schema->toSql($this->connection->getDatabasePlatform()));

    /*
     * Job
     */
    $schema = new Schema();
    $job = $schema->createTable("job");
    $job->addColumn('job_id', 'bigint', ['unsigned' => true, 'notnull' => true,'autoincrement' => true ]);
    $job->addColumn('client_job_id', 'string', ['length' => 255, 'notnull' => true ]);
    $job->addColumn('date_originally_submitted', 'datetime', ['notnull' => true]);
    $job->addColumn('max_retries', 'integer', ['notnull' => true,'default' => 0]);
    $job->addColumn('retries', 'integer', ['notnull' => true,'default' => 0]);
    $job->addColumn('next_retry', 'datetime', ['notnull' => true]);
    $job->addColumn('class_name', 'string', ['length' => 255, 'notnull' => true ]);
    $job->addColumn('parameters', 'text', ['notnull' => false ]);
    $job->addColumn('locked', 'string', ['length' => 100, 'notnull' => false ]);
    $job->addColumn('queue_id', 'bigint', ['unsigned' => true, 'notnull' => true ]);
    $job->addForeignKeyConstraint($queue, ["queue_id"], ["queue_id"], [], 'fk_queue_id');
    $job->setPrimaryKey(['job_id']);
    $job->addIndex(['client_job_id'], 'k_client_job_id');

    $sql = array_merge($sql, $schema->toSql($this->connection->getDatabasePlatform()));

    foreach ($sql as $thisSql) {
      $this->connection->executeQuery($thisSql);
    }

    $this->createQueue($_ENV['FAILED_JOB_QUEUE'], ['direction' => 'FIFO']);
    $this->createQueue('low_priority', ['direction' => 'FIFO']);
  }

  /*
   * Functions not part of the interface
   */

   /**
    * Simple check to see if a table exists. Mainly used to see if Queue exists
    * so we know the schema has been created in thei database.
    */
  protected function doesTableExist(string $tableName): bool
  {
    return count(
        array_filter(
            $this->connection->getSchemaManager()->listTables(),
            function ($v) use ($tableName) {
              return $v->getName() === $tableName;
            }
        )
    ) > 0;
  }

  /**
   * convert objectProperties to object_properties
   */
  protected function innerCapsToSnakeCase(string $value): string
  {
    return strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $value));
  }

  protected function fetchQueueId(string $queueName): int
  {
    return (int)$this->connection->createQueryBuilder()
      ->select('queue_id')
      ->from('queue')
      ->where('queue_name = :queue_name')
      ->setParameter('queue_name', $queueName)
      ->fetchOne();
  }
}
