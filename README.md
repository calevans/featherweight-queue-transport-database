# Featherweight Queue Trasnport - Database<br />
Cal Evans <cal@calevans.com>

This is the database transport for Featherweight Queue.

# Configuration

Add these entries to your `.env` file.

```
TRANSPORT_DATABASE_TYPE=mysql
TRANSPORT_DATABASE_HOST=database
TRANSPORT_DATABASE_PORT=3306
TRANSPORT_DATABASE_DATABASE=lamp
TRANSPORT_DATABASE_USER=lamp
TRANSPORT_DATABASE_PASSWORD=lamp
```



